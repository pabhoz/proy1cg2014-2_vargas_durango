var Vec4 = function( x, y, z ) {
        var x = x || 0;
        this.set( x, (y !== 0) ? y || x : 0, (z !== 0) ? z || x : 0 );
    };
 Vec4.prototype = {
        set: function( x, y, z ) {
            this.x = x;
            this.y =  (y !== 0) ? y : 0;
            this.z =  (z !== 0) ? z : 0;
            this.p = 1;
            return this;
        }
};

var Vec3 = function( x, y, z ) {
        var x = x || 0;
        this.set( x,  (y !== 0) ? y || x : 0, (z !== 0) ? z || x : 0 );
    };
 Vec3.prototype = {
        set: function( x, y, z ) {
            this.x = x;
            this.y =  (y !== 0) ? y : 0;
            this.z =  (z !== 0) ? z : 0;

            return this;
        },
        calcularModulo : function(){
         	return Math.sqrt((Math.pow(this.x,2))+(Math.pow(this.y,2))+(Math.pow(this.z,2))); 
         },
        calcularModulo2 : function(v){
            this.v = v;
            return Math.sqrt((Math.pow((v.x - this.x),2))+(Math.pow((v.y - this.y),2))+(Math.pow((v.z - this.z),2))); 
        },
        calcularDireccion : function(n){
         	var m = this.calcularModulo();
            return Grados(Math.acos(n/m));
         },
        calcularAngulo : function(v2){
                var m1 = this.calcularModulo();
                var m2 = v2.calcularModulo();
                var proP = ((this.x * v2.x)+(this.y * v2.y)+(this.z * v2.z));                
            return  Grados(Math.acos(proP/(m1*m2)));
        }
        
};

var Vec2 = function( x, y ) {
    var x = x ||  0;
        this.set( x, y||x);
    };
 Vec2.prototype = {
        set: function( x, y) {
            this.x = x;
            this.y = y || x;

            return this;
        },
        calcularModulo:  function(){
         	return Math.sqrt((Math.pow(this.x,2))+(Math.pow(this.y,2)));
        },
        calcularModulo2: function(v){
            this.v = v;
            return Math.sqrt((Math.pow((v.x - this.x),2))+(Math.pow((v.y - this.y),2))); 
        }, 
        calcularDireccion: function(n){
         	var m = this.calcularModulo();
         	return Grados(Math.acos(n/m));
        },
        calcularAngulo: function(v2){
            var result;
            var div = (v2.x - this.x);
            if(div == 0){
            result = "Se realizo una division por 0" ;   
            }else{
            result = Grados(Math.atan( (v2.y - this.y)/(div)));
            result = result + "°";
            }
            return result; 
        }       
};

function sumaV(v1,v2){
   	if(v1 instanceof Vec3 && v2 instanceof Vec3){
    var x = v1.x + v2.x;
	var y = v1.y + v2.y;
	var z = v1.z + v2.z;
	var V = new Vec3(x,y,z);
	}else if(v1 instanceof Vec2 && v2 instanceof Vec2) {
    var x = v1.x + v2.x;
	var y = v1.y + v2.y;
	var V = new Vec2(x,y);
	}else{
    var V = "¡Los vectores no coinciden!";
	}
	return V;
}

function restaV(v1,v2){
	if(v1 instanceof Vec3 && v2 instanceof Vec3){
    var x = v1.x - v2.x;
	var y = v1.y - v2.y;
	var z = v1.z - v2.z;
	var V = new Vec3(x,y,z);
	}else if(v1 instanceof Vec2 && v2 instanceof Vec2) {
    var x = v1.x - v2.x;
	var y = v1.y - v2.y;
	var V = new Vec2(x,y);
	}else if(v1 instanceof Vec4 && v2 instanceof Vec4){
    var x = v1.x - v2.x;
    var y = v1.y - v2.y;
    var z = v1.z - v2.z;
    var V = new Vec4(x,y,z);
    }else{
    var V = "¡Los vectores no coinciden!";
	}
	return V;
}

function multiplicacionVectorial(v1,v2){
	if(v1 instanceof Vec3 && v2 instanceof Vec3){ 
	var x = (v1.y*v2.z) - (v2.y*v1.z);
	var y = (-1)*((v1.x*v2.z) - (v2.x*v1.z));
	var z = (v1.x*v2.y) - (v2.x*v1.y);
	var V = new Vec3(x,y,z);
    }else if(v1 instanceof Vec2 && v2 instanceof Vec2){
    v1.z=0;
    v2.z=0;
    var x = (v1.y*v2.z) - (v2.y*v1.z);
	var y = (-1)*((v1.x*v2.z) - (v2.x*v1.z));
	var z = (v1.x*v2.y) - (v2.x*v1.y);
	var V = new Vec3(x,y,z);
    }else{
    var V= "¡Los vectores no coinciden";	
    }
	return V;
}

function productoPunto(v1,v2){
    if(v1 instanceof Vec3 && v2 instanceof Vec3){
    var V = ((v1.x*v2.x)+(v1.y*v2.y)+(v1.z*v2.z));
    }else if(v1 instanceof Vec2 && v2 instanceof Vec2){
    var V = ((v1.x*v2.x)+(v1.y*v2.y));
    }else{
    var V= "¡Los vectores no coinciden";    
    }
    return V;
}

function Grados(valor){
var V=valor*(180/Math.PI);
return V;
}

function Radianes(valor){
var V=valor*(Math.PI/180);
return V;
}

function traslacion(v1,v2){
    var x = v1.x + v2.x;
    var y = v1.y + v2.y;
    var z = v1.z + v2.z;
    var result = new Vec4(x , y , z);
return result;
}

function escalacion(v1,v2){
    var x = v1.x*v2.x;
    var y = v1.y*v2.y;
    var z = v1.z*v2.z;
var result = new Vec4(x , y, z);
return result;
}

function rotacion(g,v1,eje){
var result;
if(eje == "x"){
var a = (Math.cos(g*(Math.PI/180)));
var b = (Math.sin(g*(Math.PI/180)));
var m2=new Matriz4(1,0,0,0,0,a,b,0,0,-b,a, 0, 0, 0, 0, 1);
result = multiplicacionConVector(v1,m2);
}else if(eje == "y"){
var a = (Math.cos(g*(Math.PI/180)));
var b = (Math.sin(g*(Math.PI/180)));
var m2=new Matriz4(a,0,-b,0,0,1,0,0,b,0,a, 0, 0, 0, 0, 1);
result = multiplicacionConVector(v1,m2);   
}else if(eje == "z"){
var a = (Math.cos(g*(Math.PI/180)));
var b = (Math.sin(g*(Math.PI/180)));    
var m2=new Matriz4(a,b,0,0,-b,a, 0, 0, 0,0,1,0, 0, 0, 0, 1);
result = multiplicacionConVector(v1,m2);
}else{
result = "Los Datos introducidos son incorrecto";
}
return result;
}