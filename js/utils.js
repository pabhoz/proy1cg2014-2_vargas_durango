function textTure(ts,fw,fs,ff,c,t,x,y){
    var canvas = document.createElement('canvas');
    canvas.width = ts; canvas.height = ts;
    var context = canvas.getContext('2d');
    context.font = fw+" "+fs+" "+ff;
    context.fillStyle = "rgba("+c+")";
    context.fillText(t, x, y);
    context.stroke();
    // canvas contents will be used for a texture
    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    return texture;
}

function twoPointsDistance(a,b){//d = b-a
    return new Vec3(b.x-a.x,b.y-a.y,b.z-a.z);
}
function unitario(a){
    var m = a.calcularModulo();
    return new Vec3((a.x/m) , (a.y/m) , (a.z/m));
}
function randomHexColor(){
    return '#'+Math.floor(Math.random()*16777215).toString(16);
}
function parsePromtParams(string,flag){
    value = string.split(flag);
    for (var i = 0; i < value.length; i++) {
        if(i != value.length-1) value[i] = parseFloat(value[i]);
    };
    return value;
}

/* Acciones del Vector */
        
function creacionArrow(p0,p1,valor){
        var result;
        var vectorColor = randomHexColor();
        var vMaterial = new THREE.LineBasicMaterial({
            color:vectorColor
        });
        var vectorp0p1 = twoPointsDistance(p0,p1);/* funcion de utils */
        var uVectorp0p1 = unitario(vectorp0p1); /* funcion de utils */
        var vGeometry = new THREE.Geometry();
        vGeometry.vertices.push(p0);
        vGeometry.vertices.push(p1); /* .vertices es un arreglo */
        if(valor == 1){
        var vector = new THREE.Line(vGeometry,vMaterial);
        result = vector;
        }else if(valor == 2){
        var flechaVector = new THREE.ArrowHelper(uVectorp0p1,p1,1,vectorColor,1,1); /* Vector unitario es para la direccion al arrow*/
        result = flechaVector;
        }else if(valor == 3){
        var geometry = new THREE.BoxGeometry(0.5,0.5,0.5);
        var material = new THREE.MeshBasicMaterial({
            color: vectorColor
        });
        var PuntaCubo = new THREE.Mesh(geometry,material);
        PuntaCubo.position = new THREE.Vector3(p1.x,p1.y,p1.z);
        result = PuntaCubo;
        }
        return result;    
}

function creacionArrowModific(p0,p1,Color,valor){
        var result;
        var vectorColor = Color;
        var vMaterial = new THREE.LineBasicMaterial({
            color:vectorColor
        });
        var vectorp0p1 = twoPointsDistance(p0,p1);/* funcion de utils */
        var uVectorp0p1 = unitario(vectorp0p1);     
        var vGeometry = new THREE.Geometry();
        vGeometry.vertices.push(p0);
        vGeometry.vertices.push(p1);/* .vertices es un arreglo */
        if(valor == 1){
        var
        vector = new THREE.Line(vGeometry,vMaterial);
        result = vector;
        }else if(valor == 2){
        var flechaVector = new THREE.ArrowHelper(uVectorp0p1,p1,1,vectorColor,1,1); 
        result = flechaVector;
        }else if(valor == 3){
        var geometry = new THREE.BoxGeometry(0.5,0.5,0.5);
        var material = new THREE.MeshBasicMaterial({
            color: vectorColor
        });
        var PuntaCubo = new THREE.Mesh(geometry,material);
        PuntaCubo.position = new THREE.Vector3(p1.x,p1.y,p1.z);
        result = PuntaCubo;
        }
        return result;    
}

function mensajeVector(){
    var message = 'Digite las componentes del vector 3D separadas                \
                   por comas \",\" o un escalar para asignar a las               \
                   3 componentes';
    var value = prompt(message,"0,0,0");
    if( value != null ){
        value = value.split(",");
        for (var i = 0; i < value.length; i++) {
            value[i] = parseFloat(value[i]);
        };

        if(value.length > 1){
            return new Vec3(value[0],value[1],value[2]);
        }else{
            return new Vec3(value[0]);th
        }
    }  
}

function crearVectorInterface(){
        var p1;
        var message = "Digite 1 para crear una FLECHA con 1 vector,                                   \
                       ó Digite 2 para crear una FLECHA con 2 vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var p0 = new Vec3(0,0,0);
        p1 = mensajeVector();
        var vector = creacionArrow(p0,p1,1);
        var flechaVector = creacionArrow(p0,p1,2);
        var Label = Text3D(p1);
        labels.push(Label);
        scene.add(Label);
        scene.add(vector);
        scene.add(flechaVector);
        console.log("El vector va a nacer en el punto:");
        console.log(p0);
        console.log("La punta del vector esta en el punto:");
        console.log(p1);
        scene.add(vector);   
        }else if(value == "2"){
        var p0 = mensajeVector();
        p1 = mensajeVector();
        var vector = creacionArrow(p0,p1,1);
        var flechaVector = creacionArrow(p0,p1,2);
        var Label = Text3D(p1);
        scene.add(vector);
        scene.add(flechaVector);
        labels.push(Label);
        scene.add(Label);
        console.log("El vector va a nacer en el punto:");
        console.log(p0);
        console.log("La punta del vector esta en el punto:");
        console.log(p1);
        scene.add(vector);
        }else{
        alert("Has Digitado mal, No eres tan OP");    
        }
        if(value == "1" || value == "2"){
        objetosArrow.push(vector);
        objetosPunta.push(flechaVector);
        var obj = {
        id: objetosArrow.length-1,
        name: "Arrow | Obj "+objetosArrow.length + ": ("+ p1.x + ", "+ p1.y + ", "+ p1.z + ")"
        };
        return obj;
        }     
}

function sumarVectorInterface(){
        var vectorNuevo;
        var val = true;
        var message = "Digite 1 para suma entre 2 vectores existentes,\
Digite 2 para sumar un nuevo Vector con otro Vector ya existente, \
ó Digite 3 para crear un nuevo Vector sumando 2 nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosArrow[params-1]!=null && objetosArrow[params2-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                var p1 = objetosArrow[params2-1].geometry.vertices[1];
                vectorNuevo = sumaV(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = mensajeVector();
                var p1 = objetosArrow[params-1].geometry.vertices[1];
                vectorNuevo = sumaV(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "3"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        vectorNuevo = sumaV(p0,p1);
        var p2 = new Vec3(0,0,0);
        var vector = creacionArrow(p2,vectorNuevo,1);
        var flechaVector = creacionArrow(p2,vectorNuevo,2);
        var Label = Text3D(vectorNuevo);
        scene.add(Label);
        scene.add(vector);
        scene.add(flechaVector);
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El vector resultante es:");
        console.log(vectorNuevo);
        }else{
        alert("Has digitado mal, no eres tan OP");
        }
        if((value == "1" || value == "2" || value == "3") && val == true){
        objetosArrow.push(vector);
        objetosPunta.push(flechaVector);
        labels.push(Label);
        var obj = {
        id: objetosArrow.length-1,
        name: "Arrow (+) | Obj "+objetosArrow.length + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")"
        };
        return obj;
        }  
}

function restarVectorInterface(){
        var vectorNuevo;
        var val = true;
        var message = "Digite 1 para resta entre 2 vectores existentes\
Digite 2 para restar un nuevo Vector con otro Vector ya existente, \
ó Digite 3 para crear un nuevo Vector restando 2 nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosArrow[params-1]!=null && objetosArrow[params2-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                var p1 = objetosArrow[params2-1].geometry.vertices[1];
                vectorNuevo = restaV(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = mensajeVector();
                var p1 = objetosArrow[params-1].geometry.vertices[1];
                vectorNuevo = restaV(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "3"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        vectorNuevo = restaV(p0,p1);
        var p2 = new Vec3(0,0,0);
        var vector = creacionArrow(p2,vectorNuevo,1);
        var flechaVector = creacionArrow(p2,vectorNuevo,2);
        var Label = Text3D(vectorNuevo);
        scene.add(Label);
        scene.add(vector);
        scene.add(flechaVector);
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El vector resultante es:");
        console.log(vectorNuevo);
        }else{
        alert("Has digitado mal, no eres tan OP");
        }
        if((value == "1" || value == "2" || value == "3") && val == true){
        objetosArrow.push(vector);
        objetosPunta.push(flechaVector);
        labels.push(Label);
        var obj = {
        id: objetosArrow.length-1,
        valX: vectorNuevo.x,
        valY: vectorNuevo.y,
        valZ: vectorNuevo.z,
        name: "Arrow (-) | Obj "+objetosArrow.length + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")"
        };
        return obj;
        }  

}


function multiplicacionVectorialInterface(){
        var vectorNuevo;
        var val = true;
        var message = "Digite 1 para multiplicacion vectorial entre 2 vectores existentes,                                    \
                       Digite 2 para multiplicacion vectorial de un nuevo Vector con otro Vector ya existente,                \
                       ó Digite 3 para crear un nuevo Vector multiplicando vectorialmente 2 nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosArrow[params-1]!=null && objetosArrow[params2-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                var p1 = objetosArrow[params2-1].geometry.vertices[1];
                vectorNuevo = multiplicacionVectorial(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = mensajeVector();
                var p1 = objetosArrow[params-1].geometry.vertices[1];
                vectorNuevo = multiplicacionVectorial(p0,p1);
                var p2 =new Vec3(0,0,0);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var flechaVector = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(flechaVector);                
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El vector resultante es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "3"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        vectorNuevo = multiplicacionVectorial(p0,p1);
        var p2 = new Vec3(0,0,0);
        var vector = creacionArrow(p2,vectorNuevo,1);
        var flechaVector = creacionArrow(p2,vectorNuevo,2);
        var Label = Text3D(vectorNuevo);
        scene.add(Label);
        scene.add(vector);
        scene.add(flechaVector);
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El vector resultante es:");
        console.log(vectorNuevo);
        }else{
        alert("Has digitado mal, no eres tan OP");
        }
        if((value == "1" || value == "2" || value == "3" ) && val == true){
        objetosArrow.push(vector);
        objetosPunta.push(flechaVector);
        labels.push(Label);
        var obj = {
        id: objetosArrow.length-1,
        name: "Arrow (x) | Obj "+objetosArrow.length + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")"
        };
        return obj;
        }  

}

function productoPuntoInterface(){
        var vectorNuevo;
        var message = "Digite 1 para producto punto de un Vector con otro Vector ya existente,\
 ó Digite 2 para producto punto entre 2 nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = mensajeVector();
                var p1 = objetosArrow[params-1].geometry.vertices[1];
                vectorNuevo = productoPunto(p0,p1);
                alert("El producto punto es: "+ vectorNuevo);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El producto punto es: "+ vectorNuevo);
                }else{   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        vectorNuevo = productoPunto(p0,p1);
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El producto punto es: "+ vectorNuevo);
        alert("El producto punto es: "+ vectorNuevo);
        }else{
        alert("Has digitado mal, no eres tan OP");
        } 
}

function moduloInterface(){
        var message = "Digite 1 para el Modulo de un Vector ya existente,                        \
                       ó Digite 2 para el modulo de un vector nuevo.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                alert("El Modulo es: "+ p0.calcularModulo());
                console.log("El vector tomado:");
                console.log(p0);
                console.log("El Modulo es: "+p0.calcularModulo());
                }else{
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var p0 = mensajeVector();
        alert("El Modulo es: "+ p0.calcularModulo());
        console.log("El vector tomado:");
        console.log(p0);
        console.log("El Modulo es:"+p0.calcularModulo());
        }else{
        alert("Has digitado mal, no eres tan OP");
        } 
}

function moduloEntreInterface(){
        var vectorNuevo;
        var message = "Digite 1 para modulo entre dos vectores ya existentes,                         \
                       ó Digite 2 para modulo entre dos nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el primer numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
            if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosArrow[params-1]!=null && objetosArrow[params2-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                var p1 = objetosArrow[params2-1].geometry.vertices[1];
                vectorNuevo = p0.calcularModulo2(p1);
                alert("El Modulo entre vectores es: "+ vectorNuevo);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El modulo entre vectores es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        vectorNuevo = p0.calcularModulo2(p1);
        alert("El modulo entre Vectores es: "+ vectorNuevo);
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El modulo entre vectores es:");
        console.log(vectorNuevo);
        }else{
        alert("Has digitado mal, no eres tan OP");
        } 
}

function direccionInterface(){
        var message = "Digite 1 para la direccion de un Vector ya existente,                        \
                       ó Digite 2 para la direccion de un vector nuevo.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                alert("La direccion en X es: "+ p0.calcularDireccion(p0.x)+"°");
                alert("La direccion en Y es: "+ p0.calcularDireccion(p0.y)+"°");
                alert("La direccion en Z es: "+ p0.calcularDireccion(p0.z)+"°");
                console.log("La direccion en X es: "+ p0.calcularDireccion(p0.x)+"°");
                console.log("La direccion en Y es: "+ p0.calcularDireccion(p0.y)+"°");
                console.log("La direccion en Z es: "+ p0.calcularDireccion(p0.z)+"°");
                }else{
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var p0 = mensajeVector();
        alert("La direccion en X es: "+ p0.calcularDireccion(p0.x)+"°");
        alert("La direccion en Y es: "+ p0.calcularDireccion(p0.y)+"°");
        alert("La direccion en Z es: "+ p0.calcularDireccion(p0.z)+"°");
        console.log("La direccion en X es: "+ p0.calcularDireccion(p0.x)+"°");
        console.log("La direccion en Y es: "+ p0.calcularDireccion(p0.y)+"°");
        console.log("La direccion en Z es: "+ p0.calcularDireccion(p0.z)+"°");
        }else{
        alert("Has digitado mal, no eres tan OP");
        } 
}

function anguloInterface(){
        var message = "Digite 1 para el angulo entre 2 Vectores ya existentes,                     \
                       ó Digite 2 para el angulo entre 2 nuevos Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
            if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosArrow[params-1]!=null && objetosArrow[params2-1]!=null){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                var p1 = objetosArrow[params2-1].geometry.vertices[1];
                alert("El angulo entre vectores es: "+ p0.calcularAngulo(p1)+"°");
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("El angulo entre vectores es: "+ p0.calcularAngulo(p1)+"°");
                }else{
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var p0 = mensajeVector();
        var p1 = mensajeVector();
        alert("El angulo entre vectores es: "+ p0.calcularAngulo(p1)+"°");
        console.log("El primer vector tomado:");
        console.log(p0);
        console.log("El segundo vector tomado:");
        console.log(p1);
        console.log("El angulo entre vectores es: "+ p0.calcularAngulo(p1)+"°");
        }else{
        alert("Has digitado mal, no eres tan OP");
        } 
}

function multiplicacionConVectorInterface(){
        var vectorNuevo;
        var val = true;
        var message1 = "Digite el numero del vector ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
     
        if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1] != null ){
                var p0 = objetosArrow[params-1].geometry.vertices[1];
                p0 = new Vec4(p0.x,p0.y,p0.z);
                var p1 = mensajeMatriz2();
                var p2 = new Vec3(0,0,0);
                vectorNuevo = multiplicacionConVector(p0,p1);
                var vector = creacionArrow(p2,vectorNuevo,1);
                var punta = creacionArrow(p2,vectorNuevo,2);
                var Label = Text3D(vectorNuevo);
                scene.add(Label);
                scene.add(vector);
                scene.add(punta);
                console.log("El primer vector tomado:");
                console.log(p0);
                console.log("El segundo vector tomado:");
                console.log(p1);
                console.log("La multiplicacion con vectores es:");
                console.log(vectorNuevo);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }else{
                alert('No era tan OP, vuelve a digitar');
                val = false;
            }
        if(val == true){
        objetosArrow.push(vector);
        objetosPunta.push(punta);
        labels.push(Label);
        var obj = {
        id: objetosArrow.length-1,
        name: "Arrow (VxM) | Obj "+objetosArrow.length + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")"
        };
        return obj; 
        }
}

function EdicionMensajeInterface(){
var p1;
var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        var color = "Digita el color deseado";
        var paraColor = prompt(color,"#FFFFFF");
        if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                scene.remove(objetosArrow[params-1]);
                scene.remove(objetosPunta[params-1]);
                scene.remove(labels[params+2]);
                var vec = mensajeVector();
                var vec1 = new Vec3(0,0,0); 
                var vector = creacionArrowModific(vec1,vec,paraColor,1);
                var punta = creacionArrowModific(vec1,vec,paraColor,2);
                var Label = Text3D(vec);
                objetosArrow[params-1] = vector;
                objetosPunta[params-1] = punta;
                labels[params + 2 ] = Label; 
                labels.push(Label); 
                p1 = objetosArrow[params-1].geometry.vertices[1];
                scene.add(objetosArrow[params-1]);
                scene.add(objetosPunta[params-1]);
                scene.add(labels[params+2]);
                console.log("El nuevo vector sera nacido en:");
                console.log(vec1);
                console.log("La punta del vector estara en:");
                console.log(vec);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
                var obj = {
                id: params-1,
                name: "Arrow | Obj "+ params + ": ("+ vec.x + ", "+ vec.y + ", "+ vec.z + ")"
                };
                return obj;
            }
}


/* Botones de ayuda */

function AyudaVecMensajeInterface(){
    alert("Primero: Debes de crear el Vector en la Accion Crear Vector.");
    alert("Segundo: Escojes la Accion que deseas: Suma de vectores, Resta de Vectores, \
Multiplicacion Vectorial; te indicaran por medio de Textos los pasos ha seguir, y su resultado se\
mostraran en la grafica.");
    alert("Tercero: Al escojer las Acciones: Producto Punto, Modulo, Modulo entre Vectores, \
Direccion de Vectores, Angulo entre Vectores; estas te indicaran por medio de Textos los pasos ha seguir\
y su resultado se mostrara de un Mensaje instantaneo.");
        //---------------
    console.log("Primero: Debes de crear el Vector en la Accion Crear Vector.");
    console.log("Segundo: Escojes la Accion que deseas: Suma de vectores, Resta de Vectores, \
Multiplicacion Vectorial; te indicaran por medio de Textos los pasos ha seguir, y su resultado se\
mostraran en la grafica.");
    console.log("Tercero: Al escojer las Acciones: Producto Punto, Modulo, Modulo entre Vectores, \
Direccion de Vectores, Angulo entre Vectores; estas te indicaran por medio de Textos los pasos ha seguir\
y su resultado se mostrara de un Mensaje instantaneo.");
    
}

function AyudaMatMensajeInterface(){
    alert("Primero: Debes de crear la Matrix en la Accion Crear Matrix.");
    alert("Segundo: Escojes la Accion que deseas: Suma de Matrices, Resta de Matrices, Multiplicacion\
 Constante, Multiplicacion de Matrices y Multiplicacion con Vectores; te indicaran por medio de Textos\
 los pasos ha seguir, y su resultado se mostraran en la grafica en forma de Triangulos.");
        //------
    console.log("Primero: Debes de crear la Matrix en la Accion Crear Matrix.");
    console.log("Segundo: Escojes la Accion que deseas: Suma de Matrices, Resta de Matrices, Multiplicacion\
 Constante, Multiplicacion de Matrices y Multiplicacion con Vectores; te indicaran por medio de Textos\
 los pasos ha seguir, y su resultado se mostraran en la grafica en forma de Triangulos.");
}

function AyudaFigMensajeInterface(){
    alert("Primero: Al escojer alguna Accion esta te pediran informacion para que tu ingreses, \
esta informacion se mostrara en forma de figura en la grafica. \
Por Ejemplo: si escojes un cilindro este te pedira los parametros: \
    · Radio de Arriba\
    · Radio de Abajo\
    · Alto\
    · Radio de Segmentos\
    · Alto de Segmentos\
    ");
        //------
    console.log("Primero: Al escojer alguna Accion esta te pediran informacion para que tu ingreses, \
esta informacion se mostrara en forma de figura en la grafica. \
Por Ejemplo: si escojes un cilindro este te pedira los parametros: \
    · Radio de Arriba\
    · Radio de Abajo\
    · Alto\
    · Radio de Segmentos\
    · Alto de Segmentos\
    ");      
}

function AyudaTransMensajeInterface(){
    alert("Primero: Para poder usar las transformaciones debes de crear o un vector o una matriz\
        la cuál escojeras en transformaciones Despues a lo que vallas a crear una transformacion\
    Segundo: Podras escojer matrix 2x2, 3x3 las cuales tendran sus repectivos calcules;\
    Tercera: A lo que hayas escojido le podras hacer una traslacion,el programa tambien te podra ayudar a formar rotaciones y translacionn ");
 //----
    console.log("Primero: Para poder usar las transformaciones debes de crear o un vector o una matriz\
        la cuál escojeras en transformaciones Despues a lo que vallas a crear una transformacion");
    console.log("Segundo: Podras escojer matrix 2x2, 3x3 las cuales tendran sus repectivos calcules");
    console.log("Tercera: A lo que hayas escojido le podras hacer una traslacion,el programa\
        tambien te podra ayudar a formar rotaciones y translacionn ");
}

/* Acciones Matrices */

function mensajeMatriz(vec){
    var obj = [];
    var contmatriz = [];
    var contlabel = [];
    var contpunta = [];
    var cont_vec;
    var cont_vec2;
    var label;
    var punta;
   // var vec = [];
    for(var i = 0; i < vec.length ; i++){
            if(i == 0){    
            cont_vec = creacionArrow(vec[i],vec[i],1);
            scene.add(cont_vec);
            punta = creacionArrow(vec[i],vec[i],3);
            scene.add(punta);
            label = Text3D(vec[i]);
            scene.add(label);
            contlabel.push(label);
            contpunta.push(punta);
            contmatriz.push(cont_vec);  
            }else if(i == 1){
            cont_vec  = creacionArrow(vec[i-1],vec[i],1);
            scene.add(cont_vec);
            punta = creacionArrow(vec[i-1],vec[i],3);
            scene.add(punta);
            label = Text3D(vec[i]);
            scene.add(label);
            contlabel.push(label);
            contpunta.push(punta);
            contmatriz.push(cont_vec);
            }else{
            cont_vec  = creacionArrow(vec[i-1],vec[i],1);
            cont_vec2 = creacionArrow(vec[i-2],vec[i],1);
            scene.add(cont_vec);
            scene.add(cont_vec2);
            punta = creacionArrow(vec[i-1],vec[i],3);
            scene.add(punta);
            label = Text3D(vec[i]);
            scene.add(label);
            contlabel.push(label);
            contpunta.push(punta);
            contmatriz.push(cont_vec);
            contmatriz.push(cont_vec2);
            }
        }
        obj.push(contlabel);
        obj.push(contpunta);
        obj.push(contmatriz);
        return obj;
}

function mensajeMatriz2(){
    var message = 'Digite las componentes de la matriz separadas\
                   por comas \",\" o un escalar para asignar a las\
                   componentes';
    var value = prompt(message,"0,0,0,0,0,0,0,0,0");
    if( value != null ){
        value = value.split(",");
        for (var i = 0; i < value.length; i++) {
            value[i] = parseFloat(value[i]);
        };

        if(value.length > 1){
            return new Matriz4(value[0],value[1],value[2],0,value[3],value[4],value[5],0,value[6],value[7],value[8],0,0,0,0,1);
        }else{
            return new Matriz4(value[0]);
        }
    }  
}

function crearMatrizInterface(){
    var vec = [];
    var message1 = 'Digite los vertices separadas por \";\". \
Ejemplo (1,1,1;2,2,2;3,3,3)';
        var params1 = prompt(message1,"0,0,0;0,0,0;0,0,0");
        if(params1 !=""){
            params1 = params1.split(";");
            for(var i = 0;i < params1.length;i++){
            params1[i] = params1[i].split(",");
            }
            for(var i = 0; i < params1.length ; i++){
                for(var j = 0; j < 3 ; j++){
                    params1[i][j] = parseFloat(params1[i][j]);
                }
                vec[i] = new Vec3(params1[i][0],params1[i][1],params1[i][2]);
            }
            valoresMatriz.push(vec)
;            console.log("La nueva matriz construida por triangule_strip es: ");
            console.log(vec);
            
        var objs = mensajeMatriz(vec);
                labelsMat.push(objs[0]); 
                puntaMat.push(objs[1]);
                objetosMatriz.push(objs[2]);

        }else{
            alert("Me ire mejor por Audio, vuelve a digitar");
        }
        var obj = {
        id: objetosMatriz.length-1,
        name: "Matriz Obj: "+objetosMatriz.length+"  |  "+objs[1].length+" Vertices"
        };
        return obj;
}


function sumarMatrizInterface(){
        var matrizNueva = [];
        var vec = [];
        var val = true;
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(valoresMatriz[params-1]!=null && valoresMatriz[params2-1]!=null){
                var p0 = valoresMatriz[params-1];
                var p1 = valoresMatriz[params2-1];
                console.log("La Matriz 1 construida con triangule_strip es:");
                console.log(p0);
                console.log("La Matriz 2 construida con triangule_strip es:");
                console.log(p1);
                if( p0.length == p1.length){
                    for(var i = 0; i < p0.length ; i++){
                    matrizNueva[i] = new Vec3(p0[i].x + p1[i].x,p0[i].y + p1[i].y,p0[i].z + p1[i].z);
                    }                    
                }
                console.log("La matriz resultante es:");
                console.log(matrizNueva);
                var objs = mensajeMatriz(matrizNueva);
                labelsMat.push(objs[0]); 
                puntaMat.push(objs[1]);
                objetosMatriz.push(objs[2]); 
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }else{
                alert('No eres tan OP, vuelve a digitar');
                val = false;
            }
       
        var obj = {
        id: objetosMatriz.length-1,
        name: "Matriz (+) Obj: "+objetosMatriz.length+"  |  "+objs[1].length+" Vertices"
        };
        return obj;
}

function restarMatrizInterface(){
    var matrizNueva = [];
        var vec = [];
        var val = true;
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(valoresMatriz[params-1]!=null && valoresMatriz[params2-1]!=null){
                var p0 = valoresMatriz[params-1];
                var p1 = valoresMatriz[params2-1];
                console.log("La Matriz 1 construida con triangule_strip es:");
                console.log(p0);
                console.log("La Matriz 2 construida con triangule_strip es:");
                console.log(p1);
                if( p0.length == p1.length){
                    for(var i = 0; i < p0.length ; i++){
                    matrizNueva[i] = new Vec3(p0[i].x - p1[i].x,p0[i].y - p1[i].y,p0[i].z - p1[i].z);
                    console.log(matrizNueva[i]);  
                    }                    
                }
                console.log("La matriz resultante es:");
                console.log(matrizNueva);
                var objs = mensajeMatriz(matrizNueva);
                labelsMat.push(objs[0]); 
                puntaMat.push(objs[1]);
                objetosMatriz.push(objs[2]); 
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }else{
                alert('No eres tan OP, vuelve a digitar');
                val = false;
            }
       
        var obj = {
        id: objetosMatriz.length-1,
        name: "Matriz (-) Obj: "+objetosMatriz.length+"  |  "+objs[1].length+" Vertices"
        };
        return obj;
}

function multiplicacionConstanteMatrizInterface(){
        var matrizNueva = [];
        var val = true;       
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        var message2 = "Digite el valor del escalar. (Ejemplo: 5)";
        var params2 = prompt(message2,"5");

        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(valoresMatriz[params-1]!=null){
                var p0 = valoresMatriz[params-1];
                var p1 = params2;
                    for(var i = 0; i < p0.length ; i++){
                    matrizNueva[i] = new Vec3(p0[i].x * p1,p0[i].y * p1,p0[i].z * p1); 
                    }
                console.log("La matrix:");
                console.log(p0);
                console.log("Se ha multiplicado por la constante "+ p1);
                console.log("Se genero una nueva matrix:");
                console.log(matrizNueva);                     
                var objs = mensajeMatriz(matrizNueva);
                labelsMat.push(objs[0]); 
                puntaMat.push(objs[1]);
                objetosMatriz.push(objs[2]); 
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }else{
                alert('No eres tan OP, vuelve a digitar');
                val = false;
            }
       if( val == true){
        var obj = {
        id: objetosMatriz.length-1,
        name: "Matriz (*"+p1+") Obj: "+objetosMatriz.length+"  |  "+objs[1].length+" Vertices"
        };
        return obj;
        }
}

function multiplicacionMatricesInterface(){
var matrizNueva;
        var val = true;
        var message = "Digite 1 para multiplicacion entre 2 Matrices existentes,                                        \
                       Digite 2 para mutiplicar una nueva Matriz con otra Matriz ya existente,                          \
                       ó Digite 3 para crear una nueva Matriz multiplicando 2 nuevas Matriz Vectores.";
        var value = prompt(message,"1");
        if(value == "1"){
        var message1 = "Digite el primer numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");    
        var message2 = "Digite el segundo numero del objeto ya existen (Ejemplo: 2)";
        var params2 = prompt(message2,"2");
        if(params && params2){
                params = parseFloat(params);
                params2 = parseFloat(params2);
                if(objetosMatriz[params-1]!=null && objetosMatriz[params2-1]!=null){
                var p0 = objetosMatriz[params-1];
                var p1 = objetosMatriz[params2-1];
                matrizNueva = multiplicacionMatrices(p0,p1);
                dibujarMatriz(matrizNueva);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "2"){
        var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
            if(params){
                params = parseFloat(params);
                if(objetosMatriz[params-1]!=null){
                var p0 = mensajeMatriz();
                var p1 = objetosMatriz[params-1];
                matrizNueva = multiplicacionMatrices(p0,p1);
                dibujarMatriz(matrizNueva); 
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
            }
        }else if(value == "3"){
        var p0 = mensajeMatriz();
        var p1 = mensajeMatriz();
        matrizNueva = multiplicacionMatrices(p0,p1);
        dibujarMatriz(matrizNueva); 
        }else{
        alert("Has digitado mal, no eres tan OP");
        }
        if((value == "1" || value == "2" || value == "3") && val == true){
        objetosMatriz.push(matrizNueva);
        var obj = {
        id: objetosMatriz.length-1,
        name: "Matriz (x) | Obj "+objetosMatriz.length + ": ("+ matrizNueva.elements[0] + ","+ matrizNueva.elements[4] + ","+ matrizNueva.elements[8] + ")\
        "+"| ("+ matrizNueva.elements[1] + ","+ matrizNueva.elements[5] + ","+ matrizNueva.elements[9] + ")\
        "+"| ("+ matrizNueva.elements[2] + ","+ matrizNueva.elements[6] + ","+ matrizNueva.elements[10] + ")"
        };
        return obj; 
}
}

function Text3D(v){
    var textXYZ = "(x: " + v.x + " y: " + v.y + " z: " + v.z+")";
    var LabelTexture = textTure(150,"Bold","10px","Arial","255,255,255,1",textXYZ,20,20,20);
    var Label = new THREE.Mesh(new THREE.PlaneGeometry(2,2,2),
        new THREE.MeshBasicMaterial({
            map: LabelTexture,
            transparent: true
        })
        );
    Label.position.set(v.x,v.y,v.z);
    
    return Label;
}


/* Acciones de Objetos */

function crearCuboInterface(){
    var message = "Define los parametros del Cubo:                                 \
Lar: Largo                                                                         \
Alt: Alto                                                                          \
Anch: Ancho                                                                        \
X: posicion en X                                                                   \
Y: posicion en Y                                                                   \
Z: posicion en Z                                                                   \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Lar, Alt, Anch, X, Y, Z,");
    if(params){

        var values = parsePromtParams(params,",");
        var w = values[0] || 10,
        h = values[1] || 10,
        d = values[2] || 10,
        ws = values[3] || 1,
        hs = values[4] || 1,
        ds = values[5] || 1;
        var color = values[6] || randomHexColor();

        return crearCubo(w,h,d,ws,hs,ds,color);
    }
    return true;
}

function crearCilindroInterface(){
    var message = "Define los parametros del Cilindro:                           \
Rad: Radio de Arriba                                                             \
RadBot: Radio de Abajo                                                           \
Alt: Alto                                                                        \
RadSeg: Radio del segmento                                                       \
AltSeg:Alto de Segmento                                                          \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"RadTop, RadBot, Alt,RadSeg, AltSeg,");
    if(params){

        var values = parsePromtParams(params,",");
        var Rtop = values[0] || 10,
        Rbot = values[1] || 10,
        h = values[2] || 10,
        Rseg = values[3] || 10,
        hseg = values[4] || 10,
        opende = values[5] || true;
        var color = values[6] || randomHexColor();

        return crearCilindro(Rtop,Rbot,h,Rseg,hseg,opende,color);
    }
    return false;
}


function crearEsferaInterface(){
    var message = "Define los parametros de la Esfera:                            \
Rad: Radio de la Esfera                                                           \
AnchSeg: Segmento de Anchos                                                       \
AltoSeg: Segmento de Altos                                                        \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Rad, AnchSeg, AltSeg,");
    if(params){

        var values = parsePromtParams(params,",");
        var radio = values[0] || 21,
        wSeg = values[1] || 32,
        hSeg = values[2] || 32;
        var color = values[3] || randomHexColor();

        return crearEsfera(radio,wSeg,hSeg,color);
    }
    return false;
}

function crearDonaInterface(){
    var message = "Define los parametros de la Dona:                             \
Rad: Radio de la Dona                                                            \
AnchDona: Ancho de la Dona                                                       \
RadSeg: Radio de Segmentos                                                       \
CantSeg: Cantidad de Segmentos                                                   \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Radio, AnchDona, RadSeg, CantSeg,");
    if(params){

        var values = parsePromtParams(params,",");
        var radio = values[0] || 10,
        tube = values[1] || 5,
        radSeg = values[2] || 16,
        seg = values[3] || 100;
        var color = values[4] || randomHexColor();

        return crearDona(radio,tube,radSeg,seg,color);
    }
    return false;
}

function crearPretzelInterface(){
    var message = "Define los parametros de la Pretzel:                         \
Rad: Radio de la Pretzel                                                        \
AnchPret: Ancho de Pretzel                                                      \
RadSeg: Radio de los Segmentos                                                  \
CantSeg: Cantidad de Segmentos                                                  \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Rad, AnchPret, RadSeg, CantSeg,");
    if(params){

        var values = parsePromtParams(params,",");
        var radio = values[0] || 15,
        tube = values[1] || 2,
        radSeg = values[2] || 30,
        seg = values[3] || 80;
        var color = values[4] || randomHexColor();

        return crearPretzel(radio,tube,radSeg,seg,color);
    }
    return false;
}

function crearAnilloInterface(){
    var message = "Define los parametros del Ojo de Mordor:                                      \
RadInter: Radio Interno del Ojo de Mordor                                                        \
RadExter: Radio Externo del Ojo de Mordor                                                        \
TethaSeg: Segmentos Definidos con el Tetha                                                       \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"RadInter, RadExter, Seg,");
    if(params){

        var values = parsePromtParams(params,",");
        var inrad = values[0] || 1,
        outrad = values[1] || 5,
        tethaseg = values[2] || 32;
        var color = values[3] || randomHexColor();

        return crearAnillo(inrad,outrad,tethaseg,color);
    }
    return false;
}

function crearIcosahedronInterface(){
    var message = "Define los parametros del IcosaHedro:                                          \
Rad: Radio del IcosaHedro                                                                         \
DetIco: Detalles del Icosahedro (Cantidad de interasiones)                                        \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Rad, DetIco, ");
    if(params){

        var values = parsePromtParams(params,",");
        var rad = values[0] || 4,
        detico = values[1] || 0;
        var color = values[2] || randomHexColor();
        return crearIcosahedron(rad,detico,color);
    }
    return false;
}

function crearOctahedronInterface(){
    var message = "Define los parametros del IcosaHedro:                                          \
Rad: Radio del OctsaHedro                                                                         \
DetIco: Detalles del Octsahedro (Cantidad de interasiones)                                        \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Rad, DetIco, ");
    if(params){

        var values = parsePromtParams(params,",");
        var rad = values[0] || 4,
        detico = values[1] || 0;
        var color = values[2] || randomHexColor();
        return crearOctahedron(rad,detico,color);
    }
    return false;
}

function crearTetrahedronInterface(){
    var message = "Define los parametros del IcosaHedro:                                          \
Rad: Radio del TetrasaHedro                                                                       \
DetIco: Detalles del Tetrasahedro (Cantidad de interasiones)                                      \
Nota: agrega una coma al final de ingresar los valores";
    var params = prompt(message,"Rad, DetIco, ");
    if(params){

        var values = parsePromtParams(params,",");
        var rad = values[0] || 4,
        detico = values[1] || 0;
        var color = values[2] || randomHexColor();
        return crearTetrahedron(rad,detico,color);
    }
    return false;
}

function crearCubo(w,h,d,ws,hs,ds,color){
    var geometria = new THREE.CubeGeometry(w,h,d,ws,hs,ds);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var cubo = new THREE.Mesh( geometria, material);
    THREE.FaceColors
    objetos.push(cubo);
    scene.add(cubo);
    console.log("Cubo");
    console.log("Largo: "+w+ "\
        Alto: "+h+"\
        Ancho: "+d+ "\
        Segmentos de Largo: "+ ws+ "\
        Segmentos de Alto: "+hs+"\
        Segmentos de Ancho: "+ds);
    var obj = {
        id: objetos.length-1,
        name: "Cubo | Obj "+objetos.length
    };
    return obj;
}

function crearCilindro(Rtop,Rbot,h,Rseg,hseg,opende,color){
    var geometria = new THREE.CylinderGeometry(Rtop,Rbot,h,Rseg,hseg);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var cylinder = new THREE.Mesh( geometria, material );
    objetos.push(cylinder);
    scene.add(cylinder);
    console.log("Cilindro");
    console.log("Tapa Top: "+Rtop+ "\
        Tapa Bot: "+Rbot+"\
        Alto: "+h+ "\
        Segmentos de Radios: "+ Rseg+ "\
        Segmentos de Alto: "+hseg);
    var obj = {
        id: objetos.length-1,
        name: "Cilindro | Obj "+objetos.length
    };
    return obj;
}

function crearEsfera(radio,wSeg,hSeg,color){
    var geometria = new THREE.SphereGeometry(radio,wSeg,hSeg);
    var material = new THREE.MeshBasicMaterial({
            color: color, wireframe: true
    });
    var esfera = new THREE.Mesh( geometria, material );
    objetos.push(esfera);
    scene.add(esfera);
    console.log("Esfera");
    console.log("Radio: "+radio+ "\
        Segmentos Largo: "+wSeg+"\
        Segmentos Alto: "+hSeg);
    var obj = {
        id: objetos.length-1,
        name: "Esfera | Obj "+objetos.length
    };
    return obj;
}

function crearDona(radio,tube,radSeg,seg,color){
    var geometry = new THREE.TorusGeometry( radio, tube, radSeg,seg);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var torus = new THREE.Mesh( geometry, material );
    objetos.push(torus);
    scene.add( torus );
    console.log("Dona");
    console.log("Radio: "+radio+ "\
        Grosor: "+tube+"\
        Segmentos de Radio: "+radSeg+ "\
        Segmentos: "+ seg);
    
    var obj = {
        id: objetos.length-1,
        name: "Dona | Obj "+objetos.length
    };
    return obj;
}

function crearPretzel(radio,tube,radSeg,seg,color){
    var geometry = new THREE.TorusKnotGeometry( radio, tube, radSeg,seg);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var pretzel = new THREE.Mesh( geometry, material );
    objetos.push(pretzel);
    scene.add( pretzel );
    console.log("Pretzel");
    console.log("Radio: "+radio+ "\
        Grosor: "+tube+"\
        Segmentos de Radio: "+radSeg+ "\
        Segmentos: "+ seg);
    
    var obj = {
        id: objetos.length-1,
        name: "Pretzel | Obj "+objetos.length
    };
    return obj;
}

function crearAnillo(inrad, outrad, tethaseg, color){
    var geometry = new THREE.RingGeometry( inrad, outrad, tethaseg);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var anillo = new THREE.Mesh( geometry, material );
    objetos.push(anillo);
    scene.add( anillo );
    console.log("Anillo de Mordor");
    console.log("Radio Interno: "+inrad+ "\
        Radio Externo: "+outrad+"\
        Segmentos Tetha: "+tethaseg);
    
    var obj = {
        id: objetos.length-1,
        name: "Anillo | Obj "+objetos.length
    };
    return obj;
}

function crearIcosahedron(rad, detico, color){
    var geometry = new THREE.IcosahedronGeometry(rad, detico);
    var material = new THREE.MeshBasicMaterial({
         color: color, wireframe: true
    });
    var icohedro = new THREE.Mesh( geometry, material );
    objetos.push(icohedro);
    scene.add( icohedro );
    console.log("Icosa Hedro");
    console.log("Radio: "+rad+ "\
        Detalles: "+detico);
    var obj = {
        id: objetos.length-1,
        name: "IcoHedro | Obj "+objetos.length
    };
    return obj;
}

function crearOctahedron(rad, detico, color){
    var geometry = new THREE.OctahedronGeometry(rad, detico);
    var material = new THREE.MeshBasicMaterial({
         color: color, wireframe: true
    });
    var Octhedro = new THREE.Mesh( geometry, material );
    objetos.push(Octhedro);
    scene.add( Octhedro );
    console.log("Octa Hedro");
    console.log("Radio: "+rad+ "\
        Detalles: "+detico);
    var obj = {
        id: objetos.length-1,
        name: "OctHedro | Obj "+objetos.length
    };
    return obj;
}

function crearTetrahedron(rad, detico, color){
    var geometry = new THREE.TetrahedronGeometry(rad, detico);
    var material = new THREE.MeshBasicMaterial({
         color: color, wireframe: true
    });
    var Tetrahedro = new THREE.Mesh( geometry, material );
    objetos.push(Tetrahedro);
    scene.add( Tetrahedro );
    console.log("Tetra Hedro");
    console.log("Radio: "+rad+ "\
        Detalles: "+detico);
    var obj = {
        id: objetos.length-1,
        name: "TetraHedro | Obj "+objetos.length
    };
    return obj;
}

/* Transformaciones */

function translacionInterface(){
    var matrizNueva = [];
    var vecCont = [];
    var p1;
var message2 = 'Digite 1 para trasladar un vector ya existente,\
 Digite 2 para traladar una matriz existente.';
        var params2 = prompt(message2,"1");
        if( params2 == "1"){   
var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        if(params){
                params = parseFloat(params);
                if(objetosArrow[params-1]!=null){
                scene.remove(objetosArrow[params-1]);
                scene.remove(objetosPunta[params-1]);
                scene.remove(labels[params+2]);
                var vec = mensajeVector();
                vec = new Vec4(vec.x,vec.y,vec.z);
                var matr = new Matriz4(1,0,0,0,0,1,0,0,0,0,1,0,vec.x,vec.y,vec.z,vec.p);
                var vec1 = new Vec3(0,0,0);
                var vec2 = new Vec4(objetosArrow[params-1].geometry.vertices[1].x,objetosArrow[params-1].geometry.vertices[1].y,objetosArrow[params-1].geometry.vertices[1].z);
                var vec3 = multiplicacionConVector(vec2,matr);

                blabla  = new Vec3(vec3.x,vec3.y,vec3.z);
                var vector = creacionArrow(vec1,blabla,1);
                var punta = creacionArrow(vec1,blabla,2);
                var Label = Text3D(blabla);
                objetosArrow[params-1] = vector;
                objetosPunta[params-1] = punta;
                labels[params + 2 ] = Label; 
                p1 = objetosArrow[params-1].geometry.vertices[1];
                scene.add(objetosArrow[params-1]);
                scene.add(objetosPunta[params-1]);
                scene.add(labels[params+2]);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
                console.log("El vector a trasladar es: ");
                console.log( vec2);
                console.log("El vector resultante de la translacionn es: ");
                console.log(p1);
                var objs = [];
                var obj = {
                id: params-1,
                name: "Arrow | Obj "+ params + ": ("+ p1.x + ", "+ p1.y + ", "+ p1.z + ")",
                };
                var obj2 = {
                id: params-1,
                name: "Arrow (T) | Obj "+ params + ": ("+ p1.x + ", "+ p1.y + ", "+ p1.z + ")",
                };
                objs.push(obj);
                objs.push(obj2);
                return objs;
            }else{
                alert('No eres tan OP, vuelve a digitar');
            }
        }else if(params2 == "2"){
            var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        if(params){
                params = parseFloat(params);
                if(objetosMatriz[params-1]!=null){
                   for(var i = 0; i < objetosMatriz[params-1].length ; i++){ 
                scene.remove(objetosMatriz[params-1][i]);
                    }
                    for(var i = 0; i < puntaMat[params-1].length ; i++){
                scene.remove(puntaMat[params-1][i]);
                    }
                    for(var i = 0; i < labelsMat[params-1].length ; i++){
                scene.remove(labelsMat[params-1][i]);
                    }

                var vec = mensajeVector();
                vec = new Vec4(vec.x,vec.y,vec.z);
                var matr = new Matriz4(1,0,0,0,0,1,0,0,0,0,1,0,vec.x,vec.y,vec.z,vec.p);
                    for(var i = 0; i < valoresMatriz[params-1].length ;i ++){
                        vecCont[i] = new Vec4(valoresMatriz[params-1][i].x,valoresMatriz[params-1][i].y,valoresMatriz[params-1][i].z);
                    }
                  for(var i = 0; i < valoresMatriz[params-1].length ; i++){
                    matrizNueva[i] = multiplicacionConVector(vecCont[i],matr);
                    } 
                    valoresMatriz[params-1] = matrizNueva;
                    var objs = mensajeMatriz(matrizNueva);
                    
                    labelsMat[params-1] = objs[0]; 
                    puntaMat[params-1] = objs[1];
                    objetosMatriz[params-1] = objs[2];
                    console.log("La matrix a trasladar es: ");
                    console.log( valoresMatriz);
                    console.log("La matrix resultante de la translacionn es: ");
                    console.log(vec);
                var objs2 = [];
                var obj = {
                id: params-1,
                name: "Matriz Obj: "+ params +"  |  "+objs[1].length+" Vertices",
                };
                var obj2 = {
                id: params-1,
                name: "Matriz(T) | Obj "+ params + "  |  "+objs[1].length+" Vertices",
                };

                objs2.push(obj);
                objs2.push(obj2);
                return objs2;                

                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
                
            }else{
                alert('Has digitado mal');
            }
        }else{
           alert('Has digitado mal, no eres tan OP'); 
        }
}

function escalacionInterface(){
    var matrizNueva = [];
    var vecCont = [];
    var p1;
    var message = 'Digite 1 para trasladar un vector ya existente,\
 Digite 2 para traladar una matriz existente.';
        var params = prompt(message,"1");
        if( params == "1"){

var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params1 = prompt(message1,"1");
var message2 = 'Digite las componentes del vector de escalacion separadas                \
                   por comas \",\" o un escalar para asignar a las               \
                   3 componentes';
        var params2 = prompt(message2,"0,0,0");
        if(params1 && params2){
            params2 = params2.split(",");
            for (var i = 0; i < params2.length; i++) {
            params2[i] = parseFloat(params2[i]);
            }
            if(params2.length > 1){
            var vec = new Vec3(params2[0],params2[1],params2[2]);
            }else{
            var vec = new Vec3(params2[0]);
            }
                params1 = parseFloat(params1);
                if(objetosArrow[params1-1]!=null){
                scene.remove(objetosArrow[params1-1]);
                scene.remove(objetosPunta[params1-1]);
                scene.remove(labels[params1+2]);
                vec = new Vec4(vec.x,vec.y,vec.z);
                var matr = new Matriz4(vec.x,0,0,0,0,vec.y,0,0,0,0,vec.z,0,0,0,0,1);
                var vec1 = new Vec3(0,0,0);
                var vec2 = new Vec4(objetosArrow[params1-1].geometry.vertices[1].x,objetosArrow[params1-1].geometry.vertices[1].y,objetosArrow[params1-1].geometry.vertices[1].z);
                var vec3 = multiplicacionConVector(vec2,matr);
                blabla  = new Vec3(vec3.x,vec3.y,vec3.z);
                var vector = creacionArrow(vec1,blabla,1);
                var punta = creacionArrow(vec1,blabla,2);
                var Label = Text3D(blabla);
                objetosArrow[params1-1] = vector;
                objetosPunta[params1-1] = punta;
                labels[params1 + 2 ] = Label; 
                p1 = objetosArrow[params1-1].geometry.vertices[1];
                console.log("El vector a Escalar es: ");
                console.log( vec2);
                console.log("El vector resultante de la translacionn es: ");
                console.log(p1);
                scene.add(objetosArrow[params1-1]);
                scene.add(objetosPunta[params1-1]);
                scene.add(labels[params1+2]);
                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
                var objs = [];
                var obj = {
                id: params1-1,
                name: "Arrow | Obj "+ params1 + ": ("+ p1.x + ", "+ p1.y + ", "+ p1.z + ")",
                };
                var obj2 = {
                id: params1-1,
                name: "Arrow (E) | Obj "+ params1 + ": ("+ p1.x + ", "+ p1.y + ", "+ p1.z + ")",
                };
                objs.push(obj);
                objs.push(obj2);
                return objs;
            }else{ 
                alert('Has digitado mal, no eres tan OP');
            }
        }else if(params == "2"){
            var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params = prompt(message1,"1");
        if(params){
                params = parseFloat(params);
                if(objetosMatriz[params-1]!=null){
                   for(var i = 0; i < objetosMatriz[params-1].length ; i++){ 
                console.log(objetosMatriz[params-1].length);
                scene.remove(objetosMatriz[params-1][i]);
                    }
                    for(var i = 0; i < puntaMat[params-1].length ; i++){
                scene.remove(puntaMat[params-1][i]);
                    }
                    for(var i = 0; i < labelsMat[params-1].length ; i++){
                scene.remove(labelsMat[params-1][i]);
                    }

                var vec = mensajeVector();
                vec = new Vec4(vec.x,vec.y,vec.z);
                var contenedor = [];
                var matr = new Matriz4(vec.x,0,0,0,0,vec.y,0,0,0,0,vec.z,0,0,0,0,vec.p);
                    for(var i = 0; i < valoresMatriz[params-1].length ;i ++){
                        vecCont[i] = new Vec4(valoresMatriz[params-1][i].x,valoresMatriz[params-1][i].y,valoresMatriz[params-1][i].z);
                    }
                  for(var i = 0; i < valoresMatriz[params-1].length ; i++){
                    matrizNueva[i] = multiplicacionConVector(vecCont[i],matr);
                    }
                    var vec2 = new Vec4(matrizNueva[0].x - vecCont[0].x,matrizNueva[0].y - vecCont[0].y,matrizNueva[0].z - vecCont[0].z)
                    var matr2 = new Matriz4(1,0,0,0,0,1,0,0,0,0,1,0,-vec2.x,-vec2.y,-vec2.z,-vec2.p);

                    for(var i = 0; i < valoresMatriz[params-1].length ; i++){
                    matrizNueva[i] = multiplicacionConVector(matrizNueva[i],matr2);
                    }

                    valoresMatriz[params-1] = matrizNueva;
                    var objs = mensajeMatriz(matrizNueva);
                    
                    labelsMat[params-1] = objs[0]; 
                    puntaMat[params-1] = objs[1];
                    objetosMatriz[params-1] = objs[2];
                    console.log("La matrix a Escalar es: ");
                    console.log( valoresMatriz);
                    console.log("La matrix resultante de la escalacion es: ");
                    console.log(vec);
                var objs2 = [];
                var obj = {
                id: params-1,
                name: "Matriz Obj: "+ params +"  |  "+objs[1].length+" Vertices",
                };
                var obj2 = {
                id: params-1,
                name: "Matriz(E) | Obj "+ params + "  |  "+objs[1].length+" Vertices",
                };

                objs2.push(obj);
                objs2.push(obj2);
                return objs2;                

                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }
                
            }else{
                alert('Has digitado mal');
            }
        }else{
            alert('Has dijitado mal, no eres tan OP');
        }    

}

function rotacionInterface(){
 var matrizNueva = [];
    var vecCont = [];
    var p1;
    var message = 'Digite 1 para trasladar un vector ya existente,\
 Digite 2 para traladar una matriz existente.';
        var params = prompt(message,"1");
        if( params == "1"){
var message1 = "Digite el numero del objeto ya existen (Ejemplo: 1)";
        var params1 = prompt(message1,"1");
var message2 = "Digite el el valor de theta (Ejemplo: 90)";
        var params2 = prompt(message2,"90");
var message3 = "Digite el eje de rotacion(Ejemplo: x, y ó z)";
        var params3 = prompt(message3,"x");

        if(params1 && params2 && params3){
            params1 = parseFloat(params1);
            params2 = parseFloat(params2);

            if(objetosArrow[params1-1]!=null){
                  scene.remove(objetosArrow[params1-1]);
                  scene.remove(objetosPunta[params1-1]);
                  scene.remove(labels[params1+2]);
                  var vec1 = new Vec3(0,0,0);
                  var vec2 = new Vec4(objetosArrow[params1-1].geometry.vertices[1].x,objetosArrow[params1-1].geometry.vertices[1].y,objetosArrow[params1-1].geometry.vertices[1].z);
                  var vectorNuevo = rotacion(params2,vec2,params3);
                  var vector = creacionArrow(vec1,vectorNuevo,1);
                    var punta = creacionArrow(vec1,vectorNuevo,2);
                    var Label = Text3D(vectorNuevo);
                    objetosArrow[params1-1] = vector;
                    objetosPunta[params1-1] = punta;
                    labels[params1 + 2 ] = Label;
                    console.log("El vector a Rotar ("+ params3+" ("+params2+"°)) es: ");
                    console.log( vec2);
                    console.log("La vector resultante de las rotaciones es: ");
                    console.log(vectorNuevo);
                    scene.add(objetosArrow[params1-1]);
                    scene.add(objetosPunta[params1-1]);
                    scene.add(labels[params1+2]); 
            }else{
                alert("No existe ningun Objeto con ese numero")
            }
        }else{
            alert("Los datos no pueden ser nulos");
        }
        var objs = [];
                var obj = {
                id: params1-1,
                name: "Arrow | Obj "+ params1 + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")",
                };
                var obj2 = {
                id: params1-1,
                name: "Arrow (R) (" + params3 +") | Obj "+ params1 + ": ("+ vectorNuevo.x + ", "+ vectorNuevo.y + ", "+ vectorNuevo.z + ")",
                };
                objs.push(obj);
                objs.push(obj2);
                return objs;
            }else if(params == "2"){
            var message = "Digite el numero del objeto ya existen (Ejemplo: 1)";
            var params = prompt(message,"1");
            var message2 = "Digite el el valor de theta (Ejemplo: 90)";
            var params2 = prompt(message2,"90");
            var message3 = "Digite el eje de rotacion(Ejemplo: x, y ó z)";
            var params3 = prompt(message3,"x");

        if(params && params2 && params3){
            params = parseFloat(params);
            params2 = parseFloat(params2);
                params = parseFloat(params);
                if(objetosMatriz[params-1]!=null){
                   for(var i = 0; i < objetosMatriz[params-1].length ; i++){ 
                 scene.remove(objetosMatriz[params-1][i]);
                    }
                    for(var i = 0; i < puntaMat[params-1].length ; i++){
                scene.remove(puntaMat[params-1][i]);
                    }
                    for(var i = 0; i < labelsMat[params-1].length ; i++){
                scene.remove(labelsMat[params-1][i]);
                    }

                    for(var i = 0; i < valoresMatriz[params-1].length ;i ++){
                        vecCont[i] = new Vec4(valoresMatriz[params-1][i].x,valoresMatriz[params-1][i].y,valoresMatriz[params-1][i].z);
                    }
                  for(var i = 0; i < valoresMatriz[params-1].length ; i++){
                    matrizNueva[i] = rotacion(params2,vecCont[i],params3);
                    } 
                    console.log("El Matriz a Rotar ("+ params3+" ("+params2+"°)) es: ");
                    console.log( valoresMatriz);
                    console.log("La Matriz resultante de las rotaciones es: ");
                    console.log(matrizNueva);
                    valoresMatriz[params-1] = matrizNueva;
                    var objs = mensajeMatriz(matrizNueva);
                    
                    labelsMat[params-1] = objs[0]; 
                    puntaMat[params-1] = objs[1];
                    objetosMatriz[params-1] = objs[2];

                var objs2 = [];
                var obj = {
                id: params-1,
                name: "Matriz Obj: "+ params +"  |  "+objs[1].length+" Vertices",
                };
                var obj2 = {
                id: params-1,
                name: "Matriz(R) (" + params3 +") | Obj "+ params + "  |  "+objs[1].length+" Vertices",
                };

                objs2.push(obj);
                objs2.push(obj2);
                return objs2;                

                }else{
                val = false;   
                alert("No existe ningun Objeto con ese numero");    
                }

               }else{
            alert("Los datos no pueden ser nulos");
        } 
                
        }else{
            alert('Has dijitado mal, no eres tan OP');
        }    

}

function limpiarInterface(){
   
   for(var i = 0; i < objetosMatriz.length ; i++){ 
    for(var j = 0; j < 20 ; j++){
                
                scene.remove(objetosMatriz[i][j]);
            }
                    }
                    objetosMatriz.length=0;
                    valoresMatriz.length=0;
   for(var i = 0; i < puntaMat.length ; i++){
    for(var j = 0; j < 3 ; j++){
                scene.remove(puntaMat[i][j]);
                    }
                }
                    puntaMat.length=0;
   for(var i = 0; i < labelsMat.length ; i++){
    for(var j = 0; j < 3 ; j++){
                scene.remove(labelsMat[i][j]);
                    }
                }
                    labelsMat.length=0;
    for(var i = 0; i < objetos.length ; i++){ 
               
                scene.remove(objetos[i]);
                    }
                    objetos.length=0;
    for(var i = 0; i < objetosArrow.length ; i++){ 
              
                scene.remove(objetosArrow[i]);
                    }
                    objetosArrow.length=0;
    for(var i = 0; i < objetosPunta.length ; i++){ 
              
                scene.remove(objetosPunta[i]);
                    }
                    objetosPunta.length=0;

    for(var i = 3; i < labels.length  ; i++){ 
             
                scene.remove(labels[i]);
                    }
                    labels.length=3;

                    console.log('Has borrado todos los objetos');
                    alert('Has borrado todos los objetos');

/*var labels = [];*/

}
