var Matriz2 = function( n11, n12, n21, n22 ) {

    this.elements = new Float32Array(4);

    this.set(

        ( n11 !== undefined ) ? n11 : 1, n12 || 0,
        n21 || 0, ( n22 !== undefined ) ? n22 : 1

    );

    };
 Matriz2.prototype = {

    constructor: Matriz2,

        set: function( n11, n12, n21, n22 ) {
        var te = this.elements;
        te[0] = n11; te[2] = n12;
        te[1] = n21; te[3] = n22;
        return this;
        }
};



var Matriz3 = function( n11, n12, n13, n21, n22, n23, n31, n32, n33 ) {
    this.elements = new Float32Array(9);
    this.set(

        ( n11 !== undefined ) ? n11 : 1, n12 || 0, n13 || 0,
        n21 || 0, ( n22 !== undefined ) ? n22 : 1, n23 || 0,
        n31 || 0, n32 || 0, ( n33 !== undefined ) ? n33 : 1

    );
    };
 Matriz3.prototype = {

    constructor: Matriz3,

        set: function( n11, n12, n13, n21, n22, n23, n31, n32, n33  ) {
        var te = this.elements;
        te[0] = n11; te[3] = n12; te[6] = n13;
        te[1] = n21; te[4] = n22; te[7] = n23;
        te[2] = n31; te[5] = n32; te[8] = n33;
        return this;
        }
};

var Matriz4 = function( n11, n12, n13, n14, n21, n22, n23, n24, n31, n32, n33, n34, n41, n42, n43, n44 ) {
    this.elements = new Float32Array(16);
    this.set(

        ( n11 !== undefined ) ? n11 : 1, n12 || 0, n13 || 0, n14 || 0,
        n21 || 0, ( n22 !== undefined ) ? n22 : 1, n23 || 0, n24 || 0,
        n31 || 0, n32 || 0, ( n33 !== undefined  ) ? n33 : 1, n34 || 0,
        n41 || 0, n42 || 0, n43 || 0, ( n44 !== undefined ) ? n44 : 1

    );
    };
 Matriz4.prototype = {

    constructor: Matriz4,

        set: function( n11, n12, n13, n14, n21, n22, n23, n24, n31, n32, n33, n34, n41, n42, n43, n44  ) {
        var te = this.elements;
        te[0] = n11; te[4] = n12; te[8] = n13; te[12] = n14;
        te[1] = n21; te[5] = n22; te[9] = n23; te[13] = n24;
        te[2] = n31; te[6] = n32; te[10] = n33; te[14] = n34;
        te[3] = n41; te[7] = n42; te[11] = n43; te[15] = n44;

        return this;
        }
        
};

function sumarM(m1 , m2){


    if(m1 instanceof Matriz2 && m2 instanceof Matriz2){ 
     var M = new Matriz2(4);
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] + m2.elements[i];}
    }else if(m1 instanceof Matriz3 && m2 instanceof Matriz3){
     var M = new Matriz3(9);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] + m2.elements[i];}
    }else if(m1 instanceof Matriz4 && m2 instanceof Matriz4){
      var M = new Matriz4(16);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] + m2.elements[i];}
    }else{
        var M = 'Las Matrices no Coinciden';
    }
    return M;
}
 
function restaM(m1 , m2){

    if(m1 instanceof Matriz2 && m2 instanceof Matriz2){ 
     var M = new Matriz2(4);
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] - m2.elements[i];}
    }else if(m1 instanceof Matriz3 && m2 instanceof Matriz3){
     var M = new Matriz3(9);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] - m2.elements[i];}
    }else if(m1 instanceof Matriz4 && m2 instanceof Matriz4){
      var M = new Matriz4(16);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m1.elements[i] - m2.elements[i];}
    }else{
        var M = 'Las Matrices no Coinciden';
    }
    return M;
}

function mutiplicacionConstante( m, c ){

    if(m instanceof Matriz2){ 
     var M = new Matriz2(4);
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m.elements[i] * c;}
    }else if(m instanceof Matriz3){
     var M = new Matriz3(9);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m.elements[i] * c;}
    }else if(m instanceof Matriz4){
     var M = new Matriz4(16);  
        for (var i = 0; i < M.elements.length ; i++) {
            M.elements[i] = m.elements[i] * c;}
    }else{
     var M = 'Las Matrices no Coinciden';
    }
    return M;
}

function multiplicacionMatrices(m1, m2){

    if(m1 instanceof Matriz2 && m2 instanceof Matriz2){
     var M = new Matriz2(4);
     M.elements[0]= (m1.elements[0]*m2.elements[0])+(m1.elements[2]*m2.elements[1]);
     M.elements[1]= (m1.elements[1]*m2.elements[0])+(m1.elements[3]*m2.elements[1]);
     M.elements[2]= (m1.elements[0]*m2.elements[2])+(m1.elements[2]*m2.elements[3]);
     M.elements[3]= (m1.elements[1]*m2.elements[2])+(m1.elements[3]*m2.elements[3]);
   }else if(m1 instanceof Matriz3 && m2 instanceof Matriz3){
     var M = new Matriz3(9);
     M.elements[0]= (m1.elements[0]*m2.elements[0])+(m1.elements[3]*m2.elements[1])+(m1.elements[6]*m2.elements[2]);
     M.elements[1]= (m1.elements[1]*m2.elements[0])+(m1.elements[4]*m2.elements[1])+(m1.elements[7]*m2.elements[2]);
     M.elements[2]= (m1.elements[2]*m2.elements[0])+(m1.elements[5]*m2.elements[1])+(m1.elements[8]*m2.elements[2]);
     M.elements[3]= (m1.elements[0]*m2.elements[3])+(m1.elements[3]*m2.elements[4])+(m1.elements[6]*m2.elements[5]);
     M.elements[4]= (m1.elements[1]*m2.elements[3])+(m1.elements[4]*m2.elements[4])+(m1.elements[7]*m2.elements[5]);
     M.elements[5]= (m1.elements[2]*m2.elements[3])+(m1.elements[5]*m2.elements[4])+(m1.elements[8]*m2.elements[5]);
     M.elements[6]= (m1.elements[0]*m2.elements[6])+(m1.elements[3]*m2.elements[7])+(m1.elements[6]*m2.elements[8]);
     M.elements[7]= (m1.elements[1]*m2.elements[6])+(m1.elements[4]*m2.elements[7])+(m1.elements[7]*m2.elements[8]);
     M.elements[8]= (m1.elements[2]*m2.elements[6])+(m1.elements[5]*m2.elements[7])+(m1.elements[8]*m2.elements[8]);
   }else if(m1 instanceof Matriz4 && m2 instanceof Matriz4){
     var M = new Matriz4(16);
     M.elements[0]= (m1.elements[0]*m2.elements[0])+(m1.elements[4]*m2.elements[1])+(m1.elements[8]*m2.elements[2])+(m1.elements[12]*m2.elements[3]);
     M.elements[1]= (m1.elements[1]*m2.elements[0])+(m1.elements[5]*m2.elements[1])+(m1.elements[9]*m2.elements[2])+(m1.elements[13]*m2.elements[3]);
     M.elements[2]= (m1.elements[2]*m2.elements[0])+(m1.elements[6]*m2.elements[1])+(m1.elements[10]*m2.elements[2])+(m1.elements[14]*m2.elements[3]);
     M.elements[3]= (m1.elements[3]*m2.elements[0])+(m1.elements[7]*m2.elements[1])+(m1.elements[11]*m2.elements[2])+(m1.elements[15]*m2.elements[3]);
     M.elements[4]= (m1.elements[0]*m2.elements[4])+(m1.elements[4]*m2.elements[5])+(m1.elements[8]*m2.elements[6])+(m1.elements[12]*m2.elements[7]);
     M.elements[5]= (m1.elements[1]*m2.elements[4])+(m1.elements[5]*m2.elements[5])+(m1.elements[9]*m2.elements[6])+(m1.elements[13]*m2.elements[7]);
     M.elements[6]= (m1.elements[2]*m2.elements[4])+(m1.elements[6]*m2.elements[5])+(m1.elements[10]*m2.elements[6])+(m1.elements[14]*m2.elements[7]);
     M.elements[7]= (m1.elements[3]*m2.elements[4])+(m1.elements[7]*m2.elements[5])+(m1.elements[11]*m2.elements[6])+(m1.elements[15]*m2.elements[7]);
     M.elements[8]= (m1.elements[0]*m2.elements[8])+(m1.elements[4]*m2.elements[9])+(m1.elements[8]*m2.elements[10])+(m1.elements[12]*m2.elements[11]);
     M.elements[9]= (m1.elements[1]*m2.elements[8])+(m1.elements[5]*m2.elements[9])+(m1.elements[9]*m2.elements[10])+(m1.elements[13]*m2.elements[11]);
     M.elements[10]= (m1.elements[2]*m2.elements[8])+(m1.elements[6]*m2.elements[9])+(m1.elements[10]*m2.elements[10])+(m1.elements[14]*m2.elements[11]);
     M.elements[11]= (m1.elements[3]*m2.elements[8])+(m1.elements[7]*m2.elements[9])+(m1.elements[11]*m2.elements[10])+(m1.elements[15]*m2.elements[11]);
     M.elements[12]= (m1.elements[0]*m2.elements[12])+(m1.elements[4]*m2.elements[13])+(m1.elements[8]*m2.elements[14])+(m1.elements[12]*m2.elements[15]);
     M.elements[13]= (m1.elements[1]*m2.elements[12])+(m1.elements[5]*m2.elements[13])+(m1.elements[9]*m2.elements[14])+(m1.elements[13]*m2.elements[15]);
     M.elements[14]= (m1.elements[2]*m2.elements[12])+(m1.elements[6]*m2.elements[13])+(m1.elements[10]*m2.elements[14])+(m1.elements[14]*m2.elements[15]);
     M.elements[15]= (m1.elements[3]*m2.elements[12])+(m1.elements[7]*m2.elements[13])+(m1.elements[11]*m2.elements[14])+(m1.elements[15]*m2.elements[15]);
   }else{
   var M = 'Las matrices no Coinciden';
   }
return M;
}

function multiplicacionConVector(v,m){
var result;
if(v instanceof Vec2 && m instanceof Matriz2){
var vec = new Array(v.x,v.y);
var vec2 = new Array(2);
var suma = 0;
for( var i = 0; i < 2 ; i++ ){
    for( var j = 0 ; j < 2 ; j++ ){
        if(i == 0){
        suma = suma + (vec[j]*m.elements[i+j]);
                 }else{
        suma = suma + (vec[j]*m.elements[i+j+1]);  
                 } }
    vec2[i] = suma;
    suma=0; }
result = new Vec2(vec2[0],vec2[1]);
}else if(v instanceof Vec3 && m instanceof Matriz3){
var vec = new Array(v.x,v.y,v.z);
var vec2 = new Array(3);
var suma = 0;
for( var i = 0; i < 3 ; i++ ){
    for( var j = 0 ; j < 3 ; j++ ){
        if(i == 0){
        suma = suma + (vec[j]*m.elements[i+j]);
        }else if (i == 1 ){
        suma = suma + (vec[j]*m.elements[i+j+2]);  
        }else{
        suma = suma + (vec[j]*m.elements[i+j+4]);      
        } }
    vec2[i] = suma;
    suma=0; }
result = new Vec3(vec2[0],vec2[1],vec2[2]);
}else if(v instanceof Vec4 && m instanceof Matriz4){
var vec = new Array(v.x,v.y,v.z,v.p);
var vec2 = new Array(4);
var suma = 0;
for( var i = 0; i < 4 ; i++ ){
    for( var j = 0 ; j < 4 ; j++ ){
        if(i == 0){
        suma = suma + (vec[j]*m.elements[i+j]);
        }else if (i == 1 ){
        suma = suma + (vec[j]*m.elements[i+j+3]);  
        }else if(i == 2 ){
        suma = suma + (vec[j]*m.elements[i+j+6]);      
        }else{
        suma = suma + (vec[j]*m.elements[i+j+9]);    
        } }
    vec2[i] = suma;
    suma=0; }
result = new Vec4(vec2[0],vec2[1],vec2[2],vec2[3]);

}else{
result = "El Vector y la Matriz no Coinciden";
}
return result;
}
