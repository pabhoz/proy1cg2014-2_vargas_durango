var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var labelsMat = [];
var puntaMat = [];
var objetos = [];
var objetosArrow = [];
var objetosMatriz = [];
var valoresMatriz = [];
var objetosPunta = [];
var appW = window.innerWidth * 0.75;
var appH = window.innerHeight;

function webGLStart(){
	iniciarEscena();
	ultiTiempo = Date.now();
	animarEscena();
}

var p;
console.log(Math.cos(90*(Math.PI/180)));



function iniciarEscena(){
	renderer = new THREE.WebGLRenderer(); /* ayuda a renderirar en un espacio*/
	renderer.setSize(appW, appH);/* la dimensiones de la pagina donde vamos a renderizar*/
	document.body.appendChild(renderer.domElement); /* El arbol que contiene todos los elementos dentro nuestro documento*/

	camera = new THREE.PerspectiveCamera(45, appW/appH, 1, 500);/* camara a renderizar,  */
	camera.position.set(200,200,200);
	camera.lookAt(new THREE.Vector3(0,0,0));

	scene = new THREE.Scene();

	//Iniciar controles de la camara
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement ); /* Preguntarle a pablo */

	//Grilla!
	var grilla = new THREE.GridHelper( 100, 10 ); /* tamaño, steps---es la tabla del centro */
	scene.add(grilla);

	var axes = new THREE.AxisHelper(100);/* Los ejes y el tamaño de cada uno de los ejes */
	scene.add(axes);

	var yLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","Y",64,50);
	var yLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: yLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);             
	yLabel.position.set(0,100,0);/* posicion de la Y */
	labels.push(yLabel); /* agrega el objeto al Arreglo definido anteriormente, globalmente */
	scene.add(yLabel);

	var xLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","X",64,50);
	var xLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: xLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);
	xLabel.position.set(100,0,0); /* posicion de la X*/
	labels.push(xLabel);
	scene.add(xLabel);

	var zLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","Z",64,50);
	var zLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: zLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);
	zLabel.position.set(0,0,100); /* posicion de la Y*/
	labels.push(zLabel);
	scene.add(zLabel);

		}

	function animarEscena(){
		requestAnimationFrame( animarEscena ); /* Esto actua parecido al void draw() de processing */
		renderEscena();/* encargada de dibujar todos los objetos de la escena */
		actualizarEscena(); /* se encarga de la evaluacion logica de los elementos y su posicion */
	}

	function renderEscena(){
		renderer.render( scene, camera );
	}

	function actualizarEscena(){
		var delta = (Date.now() - ultiTiempo)/1000;
		ultiTiempo = Date.now();

		for (var i = 0; i < labels.length; i++) {
			labels[i].lookAt(camera.position);
			
		}; /* hace que los labels siempre miren hacia la camara */
		


		controlCamara.update();
	}
